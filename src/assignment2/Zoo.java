package assignment2;

public class Zoo {
	private example1.BarnYard _by;
	private example1.BarnYard _by2;
	private example1.BarnYard _by3;
	public Zoo(){
		_by = new example1.BarnYard();
		_by2 = new example1.BarnYard();
		_by3 = new example1.BarnYard();
	}
	public void addThreePigs(){
		new example1.Pig();
		new example1.Pig();
		new example1.Pig();
	}
	public void addTwoButterflys(){
		new example1.Butterfly();
		new example1.Butterfly();
	}
	public void addFourChickens(){
		new example1.Chicken();
		new example1.Chicken();
		new example1.Chicken();
		new example1.Chicken();
	}			
}
