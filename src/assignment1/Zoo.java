package assignment1;

public class Zoo {
	public Zoo(){
		example1.BarnYard by1;
		by1 = new example1.BarnYard();
		example1.BarnYard by2;
		by2 = new example1.BarnYard();
		example1.BarnYard by3;
		by3 = new example1.BarnYard();
		example1.Chicken chick1;
		chick1 = new example1.Chicken();
		example1.Chicken chick2;
		chick2 = new example1.Chicken();
		example1.Butterfly butter1;
		butter1 = new example1.Butterfly();
		example1.Butterfly butter2;
		butter2 = new example1.Butterfly();
		example1.Butterfly butter3;
		butter3 = new example1.Butterfly();
		example1.Pig pg1;
		pg1 = new example1.Pig();
		example1.Pig pg2;
		pg2 = new example1.Pig();
		by1.addChicken(chick1);
		by1.addChicken(chick2);
		by2.addButterfly(butter1);
		by2.addButterfly(butter2);
		by2.addButterfly(butter3);
		by3.addPig(pg1);
		by3.addPig(pg2);
		chick1.start();
		butter1.start();
		butter2.start();
		butter3.start();
		pg1.start();
		
	}
}
