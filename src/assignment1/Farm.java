package assignment1;

public class Farm {
	public Farm(){
		example1.BarnYard by;
		by = new example1.BarnYard();
		example1.Pig pig1;
		pig1 = new example1.Pig();
		example1.Pig pig2;
		pig2 = new example1.Pig();
		example1.Chicken chick1;
		chick1 = new example1.Chicken();
		example1.Chicken chick2;
		chick2 = new example1.Chicken();
		example1.Chicken chick3;
		chick3 = new example1.Chicken();
		example1.Butterfly butter;
		butter = new example1.Butterfly();
		by.addPig(pig1);
		by.addPig(pig2);
		by.addButterfly(butter);
		by.addChicken(chick1);
		by.addChicken(chick2);
		by.addChicken(chick3);
		chick1.start();
		chick2.start();
		pig1.start();
		pig2.start();
	}
}
